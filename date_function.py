def string_date(day: int, month: int, year: int) -> str:
    months = {
        1: 'января',
        2: 'февраля',
        3: 'марта',
        4: 'апреля',
        5: 'мая',
        6: 'июня',
        7: 'июля',
        8: 'августа',
        9: 'сентября',
        10: 'октября',
        11: 'ноября',
        12: 'декабря'
    }
    month = months.get(month)
    if month:
        return f"{day} {month} {year} года"
    else:
        return "Не верно задан месяц"


if __name__ == '__main__':
    print(string_date(1, 1, 1))
    print(string_date(32, 4, 2024))
    print(string_date(2, 13, 40))
