def sieve(num: int):
    primes = []
    cur_prime = 1
    while cur_prime < (num - 1)**(1/2) :
        cur_prime += 1
        for n in primes:
            if cur_prime % n == 0:
                break
        else:
            primes.append(cur_prime)

    for n in primes:
        if num % n == 0:
            return False
    else:
        return True


if __name__ == '__main__':
    for i in range(1, 100):
        if sieve(i):
            print(i)
