def name_counter(names: tuple) -> dict:
    name_count = {}

    for name in names:
        name_count[name] = name_count.get(name, 0) + 1

    return name_count

if __name__ == '__main__':
    print(name_counter(("Иван", "Елена", "Александр", "Наталья", "Дмитрий")))
    print(name_counter(tuple()))
    print(name_counter(("Михаил", "Анна", "Владимир", "Юлия", "Артем", "Владимир", "Владимир")))
    print(name_counter(('Олег', 'Игорь', 'Анна', 'Анна', 'Игорь', 'Анна', 'Василий')))