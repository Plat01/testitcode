def name(name_dict: dict) -> str:
    name = []
    first_name = name_dict.get('first_name')
    if first_name:
        name.append(first_name)

    middle_name = name_dict.get('middle_name')
    if name and middle_name:
        name.append(middle_name)

    last_name = name_dict.get('last_name')
    if last_name:
        name.insert(0, last_name)

    if name:
        return ' '.join(name)
    else:
        return 'Нет данных'


if __name__ == '__main__':
    print(name({'first_name': 'Имя', 'last_name': 'Фамилия', 'middle_name': 'Отчество'}))
    print(name({'first_name': 'Имя', 'last_name': 'Фамилия'}))
    print(name({'last_name': 'Фамилия'}))
    print(name({'first_name': 'Имя'}))
    print(name({'middle_name': 'Отчество'}))
    print(name({'last_name': 'Фамилия', 'middle_name': 'Отчество'}))

