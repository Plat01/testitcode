def digit_filter(*args):
    digits = set()
    for el in args:
        if isinstance(el, int):
            digits.add(el)

    return sorted(list(digits))


if __name__ == '__main__':
    print(digit_filter(1, '2', 'text', 42, None, None, None, 15, True, 1, 1))
