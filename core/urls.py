from django.urls import path

from core.views import Homepage, Infopage

urlpatterns = [
    path('', Homepage.as_view()),
    path('info', Infopage.as_view()),
]
