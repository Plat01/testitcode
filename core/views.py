from django.shortcuts import render
from django.views import View


class Homepage(View):
    def get(self, request):
        context = {
            'product': 'Welcome',
            'product_discr': ' To insta marketpalce',
        }
        return render(request=request, template_name='core/main_page.html', context=context)

class Infopage(View):
    def get(self, request):
        context = {
            'title': 'info',
            'header': 'Information page',
            'content': 'some information'
        }
        return render(request=request, template_name='core/info.html', context=context)